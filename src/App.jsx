import { PDFAnnotator } from 'pdf-annotation';

import { useState, useRef, useEffect } from 'react'

const sourceSrc="https://hubble-temp.s3.ap-southeast-1.amazonaws.com/gxkh1mfyhb0dyorkxo5pq632opd0?response-content-disposition=inline%3B%20filename%3D%22a13.pdf%22%3B%20filename%2A%3DUTF-8%27%27a13.pdf&response-content-type=application%2Fpdf&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIOJCO3HP2UDGUOGA%2F20230602%2Fap-southeast-1%2Fs3%2Faws4_request&X-Amz-Date=20230602T051311Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=8306dd9c5879c240923689998197e7ea90b293bf3eef101ebf28f4879ec4a350"

function App() {
  const pdfAnnotatorRef = useRef()
  const imgRef = useRef(null)

  useEffect(() => {
    const pdfAnnotator = new PDFAnnotator(sourceSrc);
    pdfAnnotatorRef.current = pdfAnnotator
    pdfAnnotator.id = "openseadtest-some-key";
    pdfAnnotator.srcId ='hallo' 
    pdfAnnotator.width = '100%';
    pdfAnnotator.height = '100%';
    pdfAnnotator.readonly= false;

  

    const imgCur= imgRef.current
    imgCur?.appendChild(pdfAnnotatorRef.current);
    pdfAnnotator.onDrawingSelected((overlayId, annotation, drawing) => {
      console.log("onDrawingSelected", overlayId, annotation, drawing);
    });

    pdfAnnotator.onDrawingCreated((overlayId, annotation, drawing) => {
      console.log("onDrawingCreated", overlayId, annotation, drawing);
    });

    pdfAnnotator.onDrawingDeleted((overlayId, annotation, drawing) => {
      console.log("onDrawingDeleted", overlayId, annotation, drawing);
    });

    pdfAnnotator.onReady((point) => {
      console.log("ready", point);
    });
    return ()=>{
      imgCur.removeChild(pdfAnnotatorRef.current)
    }
  },[])

  const setToolText = () => {
    pdfAnnotatorRef?.current?.setCurrentTool('text', {
      fontSize: 16, fontColor:"green"
    })
  }

  const setToolFreehand=()=>{
    pdfAnnotatorRef.current?.setCurrentTool('freehand', {
      strokeColor: 'red',
    })
  }

  return (
    <div style={{ width:"100vw", height: "100vh"}}>
      annotaton

      <div ref={imgRef} className='w-[600px] h-[600px] mx-auto' />

      <div className='mx-auto flex gap-4 justify-center'>
        <button className='bg-blue-200 p-3' onClick={setToolText}>set tool text</button>
        <button className='bg-blue-200 p-3' onClick={setToolFreehand}>set freehand</button>
      </div>
    </div>
  )
}

export default App
